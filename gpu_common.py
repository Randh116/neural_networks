import tensorflow as tf
import os

# zabezpieczenie przed bledem
# "failed to create cublas handle: CUBLAS_STATUS_ALLOC_FAILED"
# "could not create cudnn handle: CUDNN_STATUS_ALLOC_FAILED"
# "tensorflow.python.framework.errors_impl.UnknownError: Failed to get convolution algorithm."
# wyjasnienie: https://www.tensorflow.org/guide/gpu?hl=en  <-- Limiting GPU memory growth
# byly problemy z alokacja pamieci

def allowMemoryGrowth():
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
            logical_gpus = tf.config.experimental.list_logical_devices('GPU')
            print(len(gpus), "Physical GPUs, ", len(logical_gpus), "Logical GPUs")

        except RuntimeError as e:
            print(e)


# XLA - accelerated linear algebra
# nowy kompilator do optymalizacji
# przyspiesza dzialanie modeli ML

def setXLAFlag():
    os.environ['TF_XLA_FLAGS'] = '--tf_xla_enable_xla_devices'
