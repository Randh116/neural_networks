import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.datasets import fashion_mnist
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from gpu_common import allowMemoryGrowth, setXLAFlag
from models import createModelConvolutional
from plot import plotLoss, plotAccuracy

# w tym programie dzieje sie to samo co w conv_fashion_nn.py
# tyle ze tutaj model jest trenowany na 'zmienionych' danych
# nazywa sie to data augmentation
# za pomoca ImageDataGenerator modyfikuje sie dataset poddajac go transformacjom geometrycznym
# pomaga to w dokladniejszym wytrenowaniu modelu
# - wzrasta accuracy i maleje loss - ze wzgledu na wieksza roznorodnosc danych

setXLAFlag()
allowMemoryGrowth()

(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

labelMap = ["T-shirt/top", "Trouser", "Pullover", "Dress", "Coat",
            "Sandal", "Shirt", "Sneaker", "Bag", "Ankle boot"]

print('Training data shape: ', train_images.shape, train_labels.shape)
print('Testing data shape: ', test_images.shape, test_labels.shape)

classes = np.unique(train_labels)
nClasses = len(classes)

print('Total number of outputs: ', nClasses)
print('Output classes: ', classes)

plt.figure(figsize=[10, 5])

plt.subplot(121)
plt.imshow(train_images[0, :, :])
plt.title("Ground Truth : {}".format(train_labels[0]))

plt.subplot(122)
plt.imshow(test_images[0, :, :])
plt.title("Ground Truth : {}".format(test_labels[0]))

plt.show()

nDims = 1
nRows, nCols = train_images.shape[1:]
train_data = train_images.reshape(train_images.shape[0], nRows, nCols, nDims)
test_data = test_images.reshape(test_images.shape[0], nRows, nCols, nDims)
input_shape = (nRows, nCols, nDims)

train_data = train_data.astype('float32')
test_data = test_data.astype('float32')

train_data /= 255
test_data /= 255

train_labels_one_hot = to_categorical(train_labels)
test_labels_one_hot = to_categorical(test_labels)

print('Original label 0: ', train_labels[0])
print('After conversion to categorical (one-hot): ', train_labels_one_hot[0])

model = createModelConvolutional(input_shape, nClasses)
batch_size = 256
epochs = 20
model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
model.summary()

# definicja obiektu ImageDataGenerator
datagen = ImageDataGenerator(
    zoom_range=0.2,  # randomly zoom into images
    rotation_range=10,  # randomly rotate images in the range (degrees, 0 to 180)
    width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
    height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
    horizontal_flip=True,  # randomly flip images
    vertical_flip=False)  # randomly flip images

# uzyte sa inne metody z powodu wykorzystania wygenerowanych danych
# fit_generator zamiast fit
# datagen.flow tworzy batch'e z wygenerowanych danych
history = model.fit_generator(datagen.flow(train_data, train_labels_one_hot, batch_size=batch_size),
                              steps_per_epoch=int(np.ceil(train_data.shape[0] / float(batch_size))),
                              epochs=epochs,
                              validation_data=(test_data, test_labels_one_hot),
                              verbose=1)

test_loss, test_accuracy = model.evaluate(test_data, test_labels_one_hot)

plotLoss(history)
plotAccuracy(history)

plt.show()
