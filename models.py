from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten


def createModelFeedforward(input_shape, nClasses):
    # z powodu przetrenowania sieci, budujemy jeszcze raz, tym razem dodajac regularyzacje.
    # zredukuje nam to powstaly blad i otrzymamy lepsze wyniki
    # wiec powtarzamy te same kroki ale z dodana regularyzacja

    model_reg = Sequential()

    model_reg.add(Dense(512, activation='relu', input_shape=(input_shape,)))
    model_reg.add(Dropout(0.5))

    model_reg.add(Dense(512, activation='relu'))
    model_reg.add(Dropout(0.5))

    model_reg.add(Dense(nClasses, activation='softmax'))

    return model_reg


def createModelFeedforwardSimple(input_shape, nClasses):
    # tworzymy siec neuronowa
    # uzywamy warstw 'Dense' poniewaz siec jest feedforward i warstwy sa scisle polaczone
    # w ostatniej warstwie uzywamy funkcji aktywacyjnej softmax poniewaz jest to problem multiklasyfikacji
    # gdyby byl binarny uzylibysmy sigmoida

    model = Sequential()

    model.add(Dense(512, activation='relu', input_shape=(input_shape,)))
    model.add(Dense(512, activation='relu'))
    model.add(Dense(nClasses, activation='softmax'))

    return model


def createModelConvolutional(input_shape, nClasses):
    model = Sequential()

    model.add(Conv2D(32, (3, 3), padding='same', activation='relu', input_shape=input_shape))
    model.add(Conv2D(32, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(nClasses, activation='softmax'))

    return model


