from tensorflow.keras.datasets import fashion_mnist
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
import matplotlib.pyplot as plt
import numpy as np
from gpu_common import allowMemoryGrowth, setXLAFlag
from models import createModelFeedforwardSimple, createModelFeedforward
from plot import plotLoss, plotAccuracy

setXLAFlag()
allowMemoryGrowth()

# zaladowanie danych z datasetu
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

# stworzenie tablicy zawierajacej etykiety do przyporzadkowania przez siec
labelMap = ["T-shirt/top","Trouser","Pullover","Dress","Coat","Sandal","Shirt","Sneaker","Bag","Ankle boot"]

print('Training data shape: ', train_images.shape, train_labels.shape)
print('Testing data shape: ', test_images.shape, test_labels.shape)

# znajdz unikalne numery przyporzadkowane pojedynczym obiektom w datasecie (0-9)
classes = np.unique(train_labels)
nClasses = len(classes)
print('Total number of outputs: ', nClasses)
print('Output classes: ', classes)

# pogladowe przedstawienie kilku probek z datasetu
# plt.figure(figsize=[10,5])
#
# plt.subplot(121)
# plt.imshow(train_images[0, :, :], cmap='gray')
# plt.title("Ground Truth: {}".format(train_labels[0]))
#
# plt.subplot(122)
# plt.imshow(test_images[0, :, :], cmap='gray')
# plt.title("Ground Truth: {}".format(test_labels[0]))
#
# plt.show()

# kazdy piksel jest kodowany jedna wartoscia w matrycy 28x28
# zamien matryce 28x28 na pojedyncza tablice 784 z wartosciami tych pikseli
# pojedyncza wartosc takiej postaci zostanie przekazana jako pojedyncza wartosc do sieci neuronowej
dimData = np.prod(train_images.shape[1:])
train_data = train_images.reshape(train_images.shape[0], dimData)
test_data = test_images.reshape(test_images.shape[0], dimData)

# przekonwertuj na float
train_data = train_data.astype('float32')
test_data = test_data.astype('float32')

# przeskaluj dane tak, aby ich wartosc byla miedzy 0 a 1
# jeden piksel ma wartosc 0-255, wiec dzielimy tak aby to skompresowac
train_data /= 255
test_data /= 255

# konwersja danych do formatu one-hot
# tylko w takiej postaci Keras potrafi przeprowadzic klasyfikacje
# zamienia dane na postac:
# np. 3 = 001000000, 5 = 000010000
train_labels_one_hot = to_categorical(train_labels)
test_labels_one_hot = to_categorical(test_labels)

print('Original label 0: ', train_labels[0])
print('After conversion to categorical (one-hot): ', train_labels_one_hot[0])

# model = createModelFeedforwardSimple(dimData, nClasses)

# # konfiguracja modelu. dobieramy optymizator i funkcje straty pod problem multiklasyfikacyjny
# model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
# model.summary()
#
# # ustawiamy trenowanie sieci.
# # siec bedzie nakarmiona danymi 20 razy oraz bedzie walidowac dane przy uzyciu danych testowych
# history = model.fit(train_data, train_labels_one_hot, batch_size=256, epochs=20, verbose=False,
#                     validation_data=(test_data, test_labels_one_hot))
#
# # ocena wynikow, wnioskowanie
# [test_loss, test_acc] = model.evaluate(test_data, test_labels_one_hot, verbose=False)
# print("Evaluation result on test_data: \nLoss = {}, \nAccuracy = {}".format(test_loss, test_acc))
#
# # sprawdzanie sieci pod wzgledem przetrenowania.
# # mozemy dokladnie przesledzic i wyswietlic proces uczenia sieci na wykresie
# # dzieki temu wiemy jak mocno i kiedy zmienialy sie poszczegolne wartosci
#
# plotLoss(history)
# plotAccuracy(history)
#
# plt.show()

model_reg = createModelFeedforward(dimData, nClasses)

model_reg.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
model_reg.summary()

history_reg = model_reg.fit(train_data, train_labels_one_hot, batch_size=256, epochs=20, verbose=1,
                    validation_data=(test_data, test_labels_one_hot))

plotLoss(history_reg)
plotAccuracy(history_reg)

plt.show()

# przetestowanie dzialania sieci na podstawie pierwszego elementu z datasetu
# zobaczymy jaki wynik zwroci i czy dziala poprawnie
testSample = test_images[0, :, :]
plt.imshow(testSample)

# przewidywanie wynikow - jaka klase najprawdopodobniej zwroci siec
label = model_reg.predict_classes(test_data[[0], :])[0]
print("\nLabel = {} \nItem = {}".format(label, labelMap[label]))

label_test = model_reg.predict_classes(test_data[[4], :])[0]
print("\nLabel_test = {} \nItem_test = {}".format(label_test, labelMap[label_test]))

# przewidywanie prawdopodobienstwa dla kazdej klasy
prob = model_reg.predict(test_data[[0], :])
labelProb = prob[0][label]
print("Probability that object is an ankle boot = {:.3f}".format(labelProb))

# testowo sprawdzam wynik dla innej klasy, ile procent prawdopodobienstwa pokaze
# pokazalo 0.000
labelProb_test = prob[0][label_test]
print("Probability that object is a shirt = {:.3f}".format(labelProb_test))

