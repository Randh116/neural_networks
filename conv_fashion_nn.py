import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.datasets import fashion_mnist
from tensorflow.keras.utils import to_categorical
from gpu_common import allowMemoryGrowth, setXLAFlag
from models import createModelConvolutional
from plot import plotLoss, plotAccuracy

# ustawienie wymaganych parametrow
setXLAFlag()
allowMemoryGrowth()

(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()
labelMap = ["T-shirt/top", "Trouser", "Pullover", "Dress", "Coat",
            "Sandal", "Shirt", "Sneaker", "Bag", "Ankle boot"]

# wymiary:
# (60000, 28, 28) (60000,)
# (10000, 28, 28) (10000,)
# zdjecia sa czarno-biale wiec jest nie ma parametru ostatniego, czyli nDim
# jest tylko jeden kanal ktory definiuje poziom szarosci piksela
print('Training data shape: ', train_images.shape, train_labels.shape)
print('Testing data shape: ', test_images.shape, test_labels.shape)

classes = np.unique(train_labels)
nClasses = len(classes)

print('Total number of outputs: ', nClasses)
print('Output classes: ', classes)

plt.figure(figsize=[10, 5])

plt.subplot(121)
plt.imshow(train_images[0, :, :])
plt.title("Ground truth: {}".format(train_labels[0]))

plt.subplot(122)
plt.imshow(test_images[0, :, :])
plt.title("Ground truth: {}".format(test_labels[0]))

plt.show()


# w tym przykladzie tez zamieniamy w wektor ale robimy to troche inaczej
# w sensie zamiast wymiary w arrayu to sa w osobnych zmiennych
# podajemy liczbe kanalow (nDim) na sztywno poniewaz zdj sa czarno-biale
# i nie zostaje on wyszczegolniony
nDims = 1
nRows, nCols = train_images.shape[1:]
print("train images shape[1:]: ", train_images.shape[1:])
train_data = train_images.reshape(train_images.shape[0], nRows, nCols, nDims)
test_data = test_images.reshape(test_images.shape[0], nRows, nCols, nDims)
input_shape = (nRows, nCols, nDims)

train_data = train_data.astype('float32')
test_data = test_data.astype('float32')

train_data /= 255
test_data /= 255

train_labels_one_hot = to_categorical(train_labels)
test_labels_one_hot = to_categorical(test_labels)

print('Original label 0: ', train_labels[0])
print('After conversion to categorical (one-hot): ', train_labels_one_hot[0])

# tworzymy siec neuronowa
# bedziemy stackowac warstwy konwolucyjne + max pooling
# pozniej dropout bedzie redukowac liczbe neuronow
# na koncu (fully-connected) ((dense + softmax)) poniewaz musi to sklasyfikowac poprawnie
# struktura modelu:
# pierwsze 2 x convolutional po 32 filtry o rozmiarze 3x3
# pozostale convolutional maja 64 filtry o rozmiarze 3x3
# po kazdej convolutional bedzie max pooling z rozmiarem okna 2x2
# dropout z 0.25 dropout ratio po max pooling
# na koncu dense + softmax

model_conv = createModelConvolutional(input_shape, nClasses)
batch_size = 256
epochs = 20
model_conv.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
model_conv.summary()

history = model_conv.fit(train_data, train_labels_one_hot,
                         batch_size=batch_size,
                         epochs=epochs,
                         verbose=1,
                         validation_data=(test_data, test_labels_one_hot))

test_loss, test_accuracy = model_conv.evaluate(test_data, test_labels_one_hot)

plotLoss(history)
plotAccuracy(history)

plt.show()

# sprawdzamy otrzymane wyniki tak jak w przypadku feedforward
# ten przyklad na normalnie ulozonym obrazie
testSample = test_data[0, :]
plt.imshow(testSample.reshape(28, 28))
plt.show()

label = model_conv.predict_classes(testSample.reshape(1, 28, 28, nDims))[0]
print("\nLabel (normal) = {} \nItem (normal) = {}".format(label, labelMap[label]))

# przesuwamy obraz w gore, zobaczymy czy teraz rozpozna
# zwykla (feedforward) siec neuronowa sobie nie radzi
# ale konwolucyjna tak
testSample_shiftUp = np.zeros(testSample.shape)
testSample_shiftUp[1:20, :] = testSample[6:25, :]
plt.imshow(testSample_shiftUp.reshape(28, 28))
plt.show()

label = model_conv.predict_classes(testSample_shiftUp.reshape(1, 28, 28, nDims))[0]
print("\nLabel (shifted up) = {} \nItem (shifted up) = {}".format(label, labelMap[label]))

# to samo sprawdzamy dla przesunietego obrazka w dol
testSample_shiftDown = np.zeros(testSample.shape)
testSample_shiftDown[10:27, :] = testSample[6:23, :]
plt.imshow(testSample_shiftDown.reshape(28, 28))
plt.show()

label = model_conv.predict_classes(testSample_shiftDown.reshape(1, 28, 28, nDims))[0]
print("\nLabel (shifted down) = {} \nItem (shifted down) = {}".format(label, labelMap[label]))

# niezaleznosc w rozpoznawaniu danego obiektu od przesuniecia (translacji) nazywa sie
# TRANSLATION INVARIANT (INVARIANCE)









