import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.utils import to_categorical
from gpu_common import allowMemoryGrowth, setXLAFlag
from models import createModelConvolutional
from plot import plotLoss, plotAccuracy

# ustawienie wymaganych parametrow
setXLAFlag()
allowMemoryGrowth()

# ladujemy inny dataset i zmieniamy tablice z etykietami
(train_images, train_labels), (test_images, test_labels) = cifar10.load_data()
labelMap = ["airplane", "automobile", "bird", "cat", "deer",
            "dog", "frog", "horse", "ship", "truck"]

# wymiary:
# (50000, 32, 32, 3) (50000, 1)
# (10000, 32, 32, 3) (10000, 1)
print('Training data shape: ', train_images.shape, train_labels.shape)
print('Testing data shape: ', test_images.shape, test_labels.shape)

classes = np.unique(train_labels)
nClasses = len(classes)

print('Total number of outputs: ', nClasses)
print('Output classes: ', classes)

plt.figure(figsize=[10, 5])

plt.subplot(121)
plt.imshow(train_images[0, :, :])
plt.title("Ground truth: {}".format(train_labels[0]))

plt.subplot(122)
plt.imshow(test_images[0, :, :])
plt.title("Ground truth: {}".format(test_labels[0]))

plt.show()

# tutaj zmienilem kod, tak zeby nDims bylo brane z shape; w tym zbiorz jest to podane
# nDim = 3 poniewaz sa trzy kanaly koloru: r g b
nRows, nCols, nDims = train_images.shape[1:]
print("train images shape[1:]: ", train_images.shape[1:])
train_data = train_images.reshape(train_images.shape[0], nRows, nCols, nDims)
test_data = test_images.reshape(test_images.shape[0], nRows, nCols, nDims)
input_shape = (nRows, nCols, nDims)

train_data = train_data.astype('float32')
test_data = test_data.astype('float32')

train_data /= 255
test_data /= 255

train_labels_one_hot = to_categorical(train_labels)
test_labels_one_hot = to_categorical(test_labels)

print('Original label 0: ', train_labels[0])
print('After conversion to categorical (one-hot): ', train_labels_one_hot[0])

model_conv = createModelConvolutional(input_shape, nClasses)
batch_size = 256
epochs = 30
model_conv.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
model_conv.summary()

history = model_conv.fit(train_data, train_labels_one_hot, batch_size=batch_size, epochs=epochs,
                         verbose=1, validation_data=(test_data, test_labels_one_hot))

plotLoss(history)
plotAccuracy(history)

plt.show()

# sa wieksze zdjecia, wiec trzeba robic reshape na 32x32
# w tamtym przykladzie byly mniejsze obrazki: 28x28
testSample = test_data[0, :]
plt.imshow(testSample.reshape(32, 32, 3))
plt.show()

label = model_conv.predict_classes(testSample.reshape(1, 32, 32, nDims))[0]
print("\nLabel (normal) = {} \nItem (normal) = {}".format(label, labelMap[label]))

testSample_shiftUp = np.zeros(testSample.shape)
testSample_shiftUp[1:20, :] = testSample[6:25, :]
plt.imshow(testSample_shiftUp.reshape(32, 32, 3))
plt.show()

label = model_conv.predict_classes(testSample_shiftUp.reshape(1, 32, 32, nDims))[0]
print("\nLabel (shifted up) = {} \nItem (shifted up) = {}".format(label, labelMap[label]))

testSample_shiftDown = np.zeros(testSample.shape)
testSample_shiftDown[10:27, :] = testSample[6:23, :]
plt.imshow(testSample_shiftDown.reshape(32, 32, 3))
plt.show()

label = model_conv.predict_classes(testSample_shiftDown.reshape(1, 32, 32, nDims))[0]
print("\nLabel (shifted down) = {} \nItem (shifted down) = {}".format(label, labelMap[label]))
