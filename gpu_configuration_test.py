import tensorflow as tf
import os

# flaga ktora nalezy ustawic, aby uniknac bledu
# "Not creating XLA devices, tf_xla_enable_xla_devices not set"
os.environ['TF_XLA_FLAGS'] = '--tf_xla_enable_xla_devices'

tf.debugging.set_log_device_placement(True)

# to generuje liste wykrytych kart graficznych mozliwych do wykorzystania
gpus = tf.config.experimental.list_logical_devices('GPU')
if gpus:
    # Replicate your computation on multiple GPUs
    # przykladowe operacje zeby sprawdzic dzialanie
    c = []
    for gpu in gpus:
        with tf.device(gpu.name):
            a = tf.constant([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]])
            b = tf.constant([[1.0, 2.0], [3.0, 4.0], [5.0, 6.0]])
            c.append(tf.matmul(a, b))

    with tf.device('/CPU:0'):
        matmul_sum = tf.add_n(c)

    print(matmul_sum)

print("Num GPUs available: ", len(tf.config.experimental.list_physical_devices('GPU')))

print("Tensorflow version: ", tf.__version__)

# poza tym naprawilem jeszcze inne bledy.
# sciagnalem CUDA 11.2 + cudNN 11.1
# wersja tensorflow to 2.4.1

# zeby wykrywalo inne rzeczy, musialem:
# - dodac do PATH adres do cudNN\bin
# - stworzyc symlink w powershell'u na cusolver innej wersji
# C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.2\bin
# New-Item -ItemType SymbolicLink -Path .\cusolver64_10.dll -Target .\cusolver64_11.dll