import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten, BatchNormalization
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.optimizers import SGD, Adam, Adagrad, RMSprop
from gpu_common import allowMemoryGrowth, setXLAFlag
from plot import plotLoss, plotAccuracy


def createModel():
    model = Sequential()

    # feature extraction part
    model.add(Conv2D(32, (3, 3), activation='relu', padding='same', input_shape=(32, 32, 3)))
    model.add(Conv2D(32, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.15))

    model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.15))

    model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.15))

    # classification part
    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(10, activation='softmax'))

    return model


setXLAFlag()
allowMemoryGrowth()

# set random seed
np.random.seed(2020)

(train_images, train_labels), (test_images, test_labels) = cifar10.load_data()

# Training data shape
train_images_shape = train_images.shape
# Training labels shape
train_labels_shape = train_labels.shape

print('Training data shape: ', train_images_shape, train_labels_shape)

# Testing data shape
test_images_shape = test_images.shape
# Testing labels shape
test_labels_shape = test_labels.shape

print('Testing data shape: ', test_images_shape, test_labels_shape)

# Change to float datatype
train_data = train_images.astype('float32')
test_data = test_images.astype('float32')

# Scale the data to lie between 0 to 1
train_data /= 255
test_data /= 255

# Change the labels from integer to categorical data
train_labels_one_hot = to_categorical(train_labels)
test_labels_one_hot = to_categorical(test_labels)

#optim = SGD(lr=0.0001)
optim = Adam()
#optim = Adagrad()
#optim = RMSprop()

model = createModel()
model.compile(optimizer=optim, loss='categorical_crossentropy', metrics=['accuracy'])
model.summary()

history = model.fit(train_data, train_labels_one_hot,
                    epochs=5,
                    batch_size=64,
                    validation_data=(test_data, test_labels_one_hot),
                    verbose=1,
                    shuffle=False)

plotLoss(history)
plotAccuracy(history)

plt.show()